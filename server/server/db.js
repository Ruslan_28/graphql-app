const mongoose = require("mongoose")

mongoose.connect('mongodb://localhost:27017/film_db', {useNewUrlParser: true})

const dbConnection = mongoose.connection;
dbConnection.on('error', err => console.log(`Connection error: ${err}`));
dbConnection.once('open', () => console.log('Connected to DB!'));
