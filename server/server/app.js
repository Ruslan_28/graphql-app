const express = require('express');
const graphqlHTTP = require('express-graphql');
const schema = require('../schema/schema');
const cors = require('cors');
require('./db');

const app = express();
const PORT = 3005;

app.use(cors())

app.use('/graphql', graphqlHTTP({
  schema,
  graphiql: true,
}));

app.listen(PORT, err => {
  err ? console.log(err) : console.log('Server started!');
});
