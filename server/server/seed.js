const Director = require('../models/Director')
require('./db')

const defaultThen = data => console.log('ok: ', data)

const directorsToSeed = [
  { "name": "Quentin Tarantino", "age": 55 },
  { "name": "Michael Radford", "age": 72 },
  { "name": "James McTeigue", "age": 51 },
  { "name": "Guy Ritchie", "age": 50 }
]

async function seedEntities() {
  await Director.create(directorsToSeed).then(defaultThen)
}

async function clearEntites() {
  // removes all documents
  await Director.deleteMany({}).then(defaultThen)
}
clearEntites()
seedEntities()
