import { gql } from 'apollo-boost';

export const deleteMovieMutation = gql`
  mutation delteMovie($id: ID) {
    deleteMovie(id: $id) {
      id
    }
  }
`
