import { withStyles } from '@material-ui/core/styles';
import { compose } from 'recompose';
import { graphql } from 'react-apollo';

import { addMovieMutation, updateMovieMutation } from './Mutations';
import { moviesQuery } from '../MoviesTable/Queries';
import { directorsQuery } from './Queries';

import { styles } from './styles';

const withGraphqlAdd = graphql(addMovieMutation, {
  props: ({ mutate }) => ({
    addMovie: movie => mutate({
      variables: movie,
      refetchQueries:  [{ query: moviesQuery }]
    })
  })
})

const withGraphqlUpdate = graphql(updateMovieMutation, {
  props: ({ mutate }) => ({
    updateMovie: movie => mutate({
      variables: movie,
      refetchQueries:  [{ query: moviesQuery }]
    })
  })
})

export default compose(withStyles(styles), withGraphqlAdd, withGraphqlUpdate, graphql(directorsQuery));
