import { gql } from 'apollo-boost';

export const addMovieMutation = gql`
  mutation addMovie($name: String!, $rate: Int, $genre: String!, $watched: Boolean!, $directorId: ID) {
    addMovie(name: $name, genre: $genre, rate: $rate, watched: $watched, directorId: $directorId) {
      name
    }
  }
`

export const updateMovieMutation = gql`
  mutation updateMovie($id: ID, $name: String!, $rate: Int, $genre: String!, $watched: Boolean!, $directorId: ID) {
    addMovie(id: $id, name: $name, genre: $genre, rate: $rate, watched: $watched, directorId: $directorId) {
      name
    }
  }
`
